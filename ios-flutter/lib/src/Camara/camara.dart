import 'dart:io';

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

class Camara extends StatefulWidget {
  final Widget child;

  Camara({Key key, this.child}) : super(key: key);

  _CamaraState createState() => _CamaraState();
}

class _CamaraState extends State<Camara> {
  List<CameraDescription> _cameras;

  String imagePath;

  CameraController _controller;

  @override
  void initState() {
    super.initState();
    availableCameras().then((cams) {
      _cameras = cams;
      _controller = new CameraController(_cameras[0], ResolutionPreset.high);
      _controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  /// en el caso de que se tome una imagen
  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });
      }
      Navigator.pop(context, imagePath);
    });
  }

  ///toma la foto
  Future<String> takePicture() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures';
    await Directory(dirPath).create(recursive: true);
    final String filePath =
        '$dirPath/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';

    if (_controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await _controller.takePicture(filePath);
    } on CameraException catch (e) {
      print(e.description);
      return null;
    }
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> columnChildren = <Widget>[];

    if (_controller != null && _controller.value.isInitialized) {
      final size = MediaQuery.of(context).size;
      final deviceRatio = size.width / size.height;
      columnChildren.add(AspectRatio(
        child: CameraPreview(_controller),
        aspectRatio: deviceRatio,
      ));

      // final size = MediaQuery.of(context).size;
      // final deviceRatio = size.width / (size.height/2);
      // return Transform.scale(scale: _controller.value.aspectRatio / deviceRatio,
      // child: Center(child: AspectRatio(aspectRatio: _controller.value.aspectRatio,
      // child: CameraPreview(_controller),

      // ),
      // ),
      // );
    } else {
      columnChildren.add(Scaffold(
        body: Column(
          children: <Widget>[
            Text(
              "No se pudo abrir la cámara",
              style: Theme.of(context).textTheme.display3,
            ),
            Icon(
              Icons.error,
              size: 50,
            ),
          ],
        ),
      ));
    }

    return Stack(alignment: Alignment.bottomCenter, children: <Widget>[
      Column(
        children: columnChildren,
      ),
      GestureDetector(
        child: Icon(
          Icons.camera,
          size: 100,
          color: Colors.white,
        ),
        onTap: onTakePictureButtonPressed,
      ),
    ]);
  }
}

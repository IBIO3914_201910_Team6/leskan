
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
class FuncionesFirebase {
  final COLLECTION_NAME = 'USER';
  final TRACING_COLLECTION = 'TRACING';
  final String CONSULTATION_COLLECTION = 'CONSULTATION';
  
  darImageUrl(
    String seguimientoNombre,
    String conid,
  ) async{
    
    var auth = new FireBaseAuth();
    if (auth.isLoggedin()) {
   var user = await auth.getUser();
      var uid = user.uid;

      
      Map<String, dynamic> data2 = {
        "uid": uid,
        "seguimiento": [seguimientoNombre],
        "consultation": conid,
      };
      String url='';
    CloudFunctions.instance
    .call(functionName:'darURLImagenSeguimiento',parameters: data2)
    .then((resp)=>
    {
          url = resp
          
        
    });
    }
  }
}

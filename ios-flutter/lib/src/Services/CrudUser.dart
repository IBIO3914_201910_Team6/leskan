import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';

class CrudMethodsUser {

  Future addUser(Map<String, dynamic> userProfile, String uid) async {
    var auth = new FireBaseAuth();
    if (auth.isLoggedin()) {
      Firestore.instance.collection('USER').document(uid).setData(userProfile);
    }
  }

  Future<User> setUser(String uid, User user) async {
    DocumentSnapshot doc =
        await Firestore.instance.collection('USER').document(uid).get();
    user.skinToneImage = doc.data['skinToneImage'];
    user.skinTone = doc.data['skinTone'];
    user.email = doc.data['email'];
    user.firstName = doc.data['firstName'];
    user.lastName = doc.data['lastName'];
    user.sex = doc.data['sex'];
    user.birthDay = doc.data['birthDate'];
    user.commonIssue = doc.data['commonIssue'];
    user.familySkinCancer = doc.data['familySkinCancer'];
    user.nombreDoctor = doc.data['nombreDoctor'];
    user.emailDoctor = doc.data['emailDoctor'];

    return user;
  }

  Future update(String uid,Map<String, dynamic> data) async{
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }
  Future addHistory(bool commonIssue, bool familySkinCancer,String imageURL, String uid)async{
    Map<String, dynamic> data = {
      'familySkinCancer':familySkinCancer,
      'commonIssue': commonIssue,
      'skinToneImage':imageURL
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

  Future editProfileName(String firstName, String uid) async {
    Map<String, dynamic> data = {
      'firstName' : firstName,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

  Future editProfileLastName(String lastName, String uid) async {
    Map<String, dynamic> data = {
      'lastName' : lastName,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

  Future editProfileSkinTone(String skinTone, String uid)  async {
    Map<String, dynamic> data = {
      'skinTone' : skinTone,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

    Future editProfileSex(String sex, String uid)  async {
    Map<String, dynamic> data = {
      'sex' : sex,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

    Future editProfileBirthdate(String birthDate, String uid)  async {
    Map<String, dynamic> data = {
      'birthdate' : birthDate,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

  Future editDoctorName(String nombreDoctor, String uid)  async {
    Map<String, dynamic> data = {
      'nombreDoctor' : nombreDoctor,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }

   Future editDoctorEmail(String emailDoctor, String uid)  async {
    Map<String, dynamic> data = {
      'emailDoctor' : emailDoctor,
    };
    Firestore.instance.collection('USER').document(uid).updateData(data);
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';

class CrudMethodsConsulta {
  addConsulta(Consulta consulta, String uid) {
    Map<String, dynamic> data = {
      'city': consulta.city,
      'country': consulta.country,
      'date': consulta.date,
      'image': consulta.image,
      'state': consulta.state,
      
    };

    var auth = new FireBaseAuth();
    if (auth.isLoggedin()) {
      Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .document(consulta.indicador)
          .collection("CONSULTATION")
          .document()
          .setData(data);
    }
  }
}

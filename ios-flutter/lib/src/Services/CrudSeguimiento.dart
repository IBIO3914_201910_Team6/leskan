import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';

class CrudMethodsSeguimiento {
  addSeguimiento(Seguimiento seguimiento, String uid) {
    Map<String, dynamic> data = {
      'colorChange': seguimiento.colorChange,
      'name': seguimiento.name,
      'sizeChange': seguimiento.sizeChange,
      'state': seguimiento.state,
      'textureChange': seguimiento.textureChange,
      'date':seguimiento.date
    };
    var auth = new FireBaseAuth();
    if (auth.isLoggedin()) {
      Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .document(seguimiento.name)
          .setData(data);
    }

  }

  Future<User> getSeguimientos(String uid, User user) async {
    var auth = new FireBaseAuth();

    if (auth.isLoggedin()) {
      QuerySnapshot querySnapshot = await Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .getDocuments();
      var list = querySnapshot.documents;

      for (var segumiento in list) {
        Seguimiento nvo = new Seguimiento();
        nvo.name = segumiento.documentID;
        nvo.date = segumiento.data['date'] as int;
        nvo.textureChange = segumiento.data['textureChange'] as String;
        nvo.colorChange = segumiento.data['colorChange'] as String;
        nvo.state = segumiento.data['state'];
        QuerySnapshot querySnapshot2 = await Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .document(segumiento.documentID).collection('CONSULTATION').getDocuments();
        var conslist = querySnapshot2.documents;
        for (var cons in conslist) {
          Consulta nvaCons = new Consulta();
          nvaCons.indicador = cons.data['id'];
          nvaCons.city = cons.data['city'];
          nvaCons.country = cons.data['country'];
          nvaCons.date = cons.data['date'];
          nvaCons.state = cons.data['state'];
          nvaCons.image = cons.data['image'];
          nvo.addConsulta(nvaCons);
        }
        user.addSeguimiento(nvo);
      }
      return user;
    }
    return null;
  }
}

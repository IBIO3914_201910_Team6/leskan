import 'dart:core';
import 'package:flutter/material.dart';

class BuildTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final bool obscure;
  final String hint;
  final String label;
  final FormFieldSetter<String> onSaved;
  final BuildContext context;

  BuildTextFormField(this.controller, this.validator, this.obscure, this.hint,
      this.label, this.onSaved, this.context,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.black),
        child: TextFormField(
          style: TextStyle(
            //color: Colors.white,
          ),
          controller: controller,
          obscureText: obscure,
          validator: validator,
          maxLength: 40,
          decoration: new InputDecoration(
              isDense: true,
              hintText: hint,
              labelText: label,
              hintStyle: TextStyle(
            //color: Colors.white,
          ),
              ),
          onSaved: onSaved,
        ));
  }
}

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ios_flutter/src/Tabs/Profile.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';

class EditarPerfil extends StatefulWidget {
  final Widget child;
  final User user;

  EditarPerfil(this.user, {Key key, this.child}) : super(key: key);

  _EditarState createState() => _EditarState(user);
}

class _EditarState extends State<EditarPerfil> {
  _EditarState(this._user);

  CrudMethodsUser _servicesUser = new CrudMethodsUser();
  TextEditingController nameController = new TextEditingController();
  User _user;
  int _sexo;
  String _nombre;
  String _apellido;
  String _nombreDoctor;
  String _emailDoctor;
  final _formNombre = GlobalKey<FormState>();
  FormState formS;
  int _lesionesFrecuentes;

  int _precedentesFamiliares;

  DateTime _birthDay = new DateTime.now();

  Future seleccioarDia(BuildContext context) async {
        final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDay,
        firstDate: DateTime(1910, 1),
        lastDate: DateTime.now());
        builder: (BuildContext context, Widget child){
          return FittedBox(
            child: Theme(
              child: child,
              data: ThemeData(
                primaryColor: Color(0xffE6CCBE),
                ),
                ),
        );
        };

    if (picked != null && picked != _birthDay)
      setState(() {
        _birthDay = picked;
      });
  }

  void handleRadioValueChange(int value) {
    setState(() {
      _sexo = value;
    });
  }

  void initState() {
    setState(() {
      _birthDay = DateTime.parse(_user.birthDay);

      _sexo = _user.sex;

      (_user.familySkinCancer)
          ? _precedentesFamiliares = 1
          : _precedentesFamiliares = 0;

      (_user.commonIssue) ? _lesionesFrecuentes = 1 : _lesionesFrecuentes = 0;

      switch (_lesionesFrecuentes) {
        case 0:
          _user.commonIssue = false;
          break;
        case 1:
          _user.commonIssue = true;
          break;
      }
    });
    super.initState();
  }

  void manejoLesionesFrecuentes(int value) {
    setState(() {
      _lesionesFrecuentes = value;
    });
  }

  void manejoAntecedentes(int value) {
    setState(() {
      _precedentesFamiliares = value;
    });
  }

  void submit() async {
    var estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      showDialog(
        context: this.context,
        builder: (context) =>
            errorMessage(context, "No esta conectado al internet"),
      );
      return;
    }
    if (this.formS == null) {
      Navigator.pop(context);
    }
    else if (this.formS.validate()) {
      formS.save();

      
      String month = _birthDay.month.toString();
      if (month.length == 1) {
        month = "0" + month;
      }

      String day = _birthDay.day.toString();
      if (day.length == 1) {
        day = "0" + day;
      }

      String nacimiento = _birthDay.year.toString() + '-' + month + '-' + day;

      _user.firstName= this._nombre==null?'':this._nombre;
      _user.lastName = this._apellido;
      _user.sex = this._sexo;
      _user.birthDay = nacimiento;
      _user.nombreDoctor = this._nombreDoctor;
      _user.emailDoctor = this._emailDoctor;
      if (_precedentesFamiliares != null) {
        if (this._precedentesFamiliares == 0) {
          _user.familySkinCancer = true;
        } else {
          _user.familySkinCancer = false;
        }
      }

      if (_lesionesFrecuentes != null) {
        if (this._lesionesFrecuentes == 0) {
          _user.commonIssue = true;
        } else {
          _user.commonIssue = false;
        }
      }
        var auth = new FireBaseAuth();
       var userAuth = await auth.getUser();
       
 Map<String, dynamic> data = {
          'firstName': this._nombre,
          'lastName': this._apellido,
          'nombreDoctor': this._nombreDoctor,
          'emailDoctor': this._emailDoctor,
          'sex': this._sexo,
          'birthDate': nacimiento,
          'skinTone': -1,
          'skinToneImage': '',
        };

     await _servicesUser.update(userAuth.uid, data);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    FireBaseAuth _auth = new FireBaseAuth();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffA64253),
        centerTitle: true,
        actions: <Widget>[],
        title: Text(
          'Editar Perfil',
          style: TextStyle(
              color: Color(0xffE6CCBE),
              fontSize: MediaQuery.of(context).size.width / 18,
              fontFamily: 'Calibri Light'),
          textAlign: TextAlign.center,
        ),
        leading: Icon(
          Icons.accessibility,
          color: Color(0xffE6CCBE),
        ),
      ),
      body: Form(
        key: _formNombre,
        autovalidate: true,
        onChanged: ()=>{
        setState((){

        formS= _formNombre.currentState;
        })
     },
        child: Container(
          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 20, right: MediaQuery.of(context).size.width / 20),
          color: Color(0xffE6CCBE),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height / 30,
              ),
              Row(
                children: <Widget>[
                  
                  Text(
                    "Nombre: ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),
                  
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onFieldSubmitted:(value) {
                          print(value);
                           setState(() { this._nombre = value;});
                            _nombre=value;
                        }, 
                        onSaved: (value) {
                          print(value);
                           setState(() { this._nombre = value;});
                         
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },
                        style: TextStyle(
                            color: Color(0xff5A5353),
                            fontSize: MediaQuery.of(context).size.width / 22,
                            fontFamily: 'Calibri Light'),
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.firstName}",
                      ),
                    ), //flexible
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 55),
              Row(
                children: <Widget>[
                  
                  Text(
                    "Apellido: ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),
                  
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onSaved: (value) {
                          _apellido = value;
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.lastName}",
                        style: TextStyle(
                            color: Color(0xff5A5353),
                            fontSize: MediaQuery.of(context).size.width / 22,
                            fontFamily: 'Calibri Light'),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 20),
              Row(
                children: <Widget>[
                  
                  Text(
                    "Sexo: ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),],),
                  Row(children: <Widget>[
                  Radio(
                    groupValue: _sexo,
                    value: 0,
                    onChanged: handleRadioValueChange,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "Femenino",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: MediaQuery.of(context).size.width / 22,
                        fontFamily: 'Calibri Light'),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 30,
                  ),
                  Radio(
                    groupValue: _sexo,
                    value: 1,
                    onChanged: handleRadioValueChange,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "Masculino",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: 16,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 30),
              Row(
                children: <Widget>[
                  
                  Text(
                    "Fecha de Nacimiento:",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
              
              SizedBox(
                height: MediaQuery.of(context).size.height / 50,
              ),
              Row(
                children: <Widget>[
                  
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
       
                    color: Color(0xffAD9B8A),
                    onPressed: () => seleccioarDia(context),
                    child: Text(
                      "Cambiar Fecha",
                      style: TextStyle(
                          color: Color(0xff5A5353),
                          fontSize: MediaQuery.of(context).size.width / 24,
                          fontFamily: 'Calibri Light'),
                    ),
                    splashColor: Color(0xffA64253),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.height / 45,
                  ),
                  Container(
                    child: (_birthDay == DateTime.now())
                        ? Text('${_user.birthDay}')
                        : Text(
                            '${_birthDay.day}/${_birthDay.month}/${_birthDay.year}',
                            style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: MediaQuery.of(context).size.width / 22,
                                fontFamily: 'Calibri Light'),
                          ),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 20),
              Row(
                children: <Widget>[
                  
                  Text(
                    "Nombre de dermatólogo: ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
             
                 Row(
                children: <Widget>[

                
                  
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onSaved: (value) {
                          _nombreDoctor = value;
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },

                        
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.nombreDoctor}",
                        style: TextStyle(
                            color: Color(0xff5A5353),
                            fontSize: MediaQuery.of(context).size.width / 22,
                            fontFamily: 'Calibri Light'),
                      ),
                    ),
                  ),
                ],
              ),

SizedBox(height: MediaQuery.of(context).size.height / 20),
            Row(
                children: <Widget>[
                  
                  Text(
                    "Correo de dermatólogo: ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 18,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
             
                 Row(
                children: <Widget>[

                
                  
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onSaved: (value) {
                          _emailDoctor = value;
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.emailDoctor}",
                        style: TextStyle(
                            color: Color(0xff5A5353),
                            fontSize: MediaQuery.of(context).size.width / 22,
                            fontFamily: 'Calibri Light'),
                      ),
                    ),
                  ),
                ],
              ),
SizedBox(height: MediaQuery.of(context).size.height / 20),
              
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  
                    Text(
                    
                    "¿Presenta lesiones de piel",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 16,
                        fontFamily: 'Calibri Light'),
                        textAlign: TextAlign.center,
                  ),
                  
                  
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  Text(
                    
                    "frecuentemente?",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 16,
                        fontFamily: 'Calibri Light'),
                        textAlign: TextAlign.center,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    groupValue: _lesionesFrecuentes,
                    value: 0,
                    onChanged: manejoLesionesFrecuentes,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "Sí",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: MediaQuery.of(context).size.width / 22,
                        fontFamily: 'Calibri Light'),
                  ),
                  SizedBox(
                    width:MediaQuery.of(context).size.width / 25,
                  ),
                  Radio(
                    groupValue: _lesionesFrecuentes,
                    value: 1,
                    onChanged: manejoLesionesFrecuentes,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "No",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: MediaQuery.of(context).size.width / 22,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  Text(
                    "¿Algún familiar suyo ha sufrido ",
                    style: TextStyle(
                        color: Color(0xffA07178),
                        fontSize: MediaQuery.of(context).size.width / 16,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
              Center(
                child: Text(
                  "de cáncer en la piel?",
                  style: TextStyle(
                      color: Color(0xffA07178),
                      fontSize: MediaQuery.of(context).size.width / 16,
                      fontFamily: 'Calibri Light'),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    groupValue: _precedentesFamiliares,
                    value: 0,
                    onChanged: manejoAntecedentes,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "Sí",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: MediaQuery.of(context).size.width / 22,
                        fontFamily: 'Calibri Light'),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 40,
                  ),
                  Radio(
                    groupValue: _precedentesFamiliares,
                    value: 1,
                    onChanged: manejoAntecedentes,
                    activeColor: Color(0xff5A5353),
                  ),
                  Text(
                    "No",
                    style: TextStyle(
                        color: Color(0xff5A5353),
                        fontSize: MediaQuery.of(context).size.width / 22,
                        fontFamily: 'Calibri Light'),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 40,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: RaisedButton(
                    child: Text(
                      "Guardar",
                      style: TextStyle(
                          color: Color(0xff5A5353),
                          fontSize: MediaQuery.of(context).size.width / 18,
                          fontFamily: 'Calibri Light'),
                    ),
                    color: Color(0xffAD9B8A),
                    elevation: 4,
                    onPressed: submit,
                  )),
                  SizedBox(
                height: MediaQuery.of(context).size.height / 40,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

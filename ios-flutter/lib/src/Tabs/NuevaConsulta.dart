import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/FireBase/firebStorage.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';

import 'package:ios_flutter/src/Services/CrudConsulta.dart';
import 'package:ios_flutter/src/Services/CrudSeguimiento.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ios_flutter/src/Tabs/Seguimientos.dart';
import 'package:ios_flutter/src/firstPage.dart';

class NuevaConsulta extends StatefulWidget {
  final Widget child;
  final User user;

  NuevaConsulta(this.user, {Key key, this.child}) : super(key: key);

  _NuevaConsultaState createState() => _NuevaConsultaState(user);
}

class _NuevaConsultaState extends State<NuevaConsulta> {
  User _user;

  String _imageURL;

  final _formKey = GlobalKey<FormState>();

  String _titulo;

  _NuevaConsultaState(this._user);

  void guardar() async {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (_imageURL == null) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Coloque una imagen"),
        ));
        var lista = _user.seguimientos;
        lista?.forEach((segimiento) {
          if (segimiento.name == _titulo) {
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("Ya hay un seguimiento con ese nombre"),
            ));

            return;
          }
        });
      } else {
        var estado = await Connectivity().checkConnectivity();
        if (estado == ConnectivityResult.none) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text("No esta conectado al internet"),
          ));
          return;
        }
        try {
          Seguimiento seg = new Seguimiento();
          Consulta cons = new Consulta();
          var auth = new FireBaseAuth();

          var userAuth = await auth.getUser();

          FirebStorage fireSto = new FirebStorage();

       
          var date = DateTime.now().millisecondsSinceEpoch;
          String tituloSinEspacios =
              _titulo.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
          String rutaGuardar = userAuth.uid +
              '/tracings/' +
              tituloSinEspacios +
              '/' +
              date.toString() +
              '.jpg';
          String imageURL = await fireSto
              .send(_imageURL, rutaGuardar)
              .timeout(const Duration(seconds: 10), onTimeout: _onTimeout);

          // agrego al modelo
          seg.name = _titulo;
          seg.colorChange = "0%";
          seg.state = "Normal";
          seg.textureChange = "0%";
          seg.sizeChange = "0%";
          seg.date = date;

          cons.city = '';
          cons.country = '';
          cons.date = date;
          cons.image = imageURL;
          cons.indicador = _titulo;
          cons.state = 'LOADING';

          seg.addConsulta(cons);

          _user.addSeguimiento(seg);

          //persisto localmente
          var repoCons =
              new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
          var repoSeg = new FuturePreferencesRepository<Seguimiento>(
              new SeguimientoDesSer());
          repoCons.save(cons);
          repoSeg.save(seg);

          // guardo en base de datos
          CrudMethodsSeguimiento crudSeg = new CrudMethodsSeguimiento();
          CrudMethodsConsulta crudCons = new CrudMethodsConsulta();

          crudSeg.addSeguimiento(seg, userAuth.uid);
          crudCons.addConsulta(cons, userAuth.uid);

          Navigator.push(context,
              MaterialPageRoute(builder: (context) => FirstPage(_user)));

          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text("¡Se añadió la consulta exitosamente!"),
          ));
        } catch (e) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(e.toString()),
          ));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Color(0xffA64253),
          automaticallyImplyLeading: false,
          title: Text(
            "Generar una nueva consulta",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color(0xffE6CCBE),
                fontSize: MediaQuery.of(context).size.width / 18,
                fontFamily: 'Calibri Light'),
          )),
      body: Container(
        color: Color(0xffE6CCBE),
        child: ListView(
          padding: EdgeInsets.all(40),
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height / 50,
            ),
            (_imageURL == null)
                ? Card(
                    color: Color(0xffE6CCBE),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                    child: SizedBox(
                      child: Center(
                        child: Text(
                          "No hay imagen seleccionada",
                          style: TextStyle(
                              color: Color(0xff5A5353),
                              fontSize: MediaQuery.of(context).size.width / 30,
                              fontFamily: 'Calibri Light'),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 3,
                    ),
                  )
                : Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Color(0xffAD9B8A),
                    elevation: 5,
                    child: SizedBox(
                      child: Image.file(File(_imageURL)),
                    ),
                  ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 25,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width / 4),
              child: RaisedButton(
                color: Color(0xffAD9B8A),
                onPressed: () async {
                  var image = await Navigator.push(
                      context,
                      MaterialPageRoute<String>(
                        builder: (context) => Camara(),
                      ));
                  setState(() {
                    _imageURL = image;
                  });
                },
                child: Icon(
                  Icons.camera_alt,
                  size: MediaQuery.of(context).size.width / 10,
                  color: Color(0xff5A5353),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 30,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  BuildTextFormField(
                      null,
                      (value) {
                        if (value.contains('.') ||
                            value.contains('/') ||
                            value.contains('\\') ||
                            value.contains('|') ||
                            value.contains(',')) {
                          return "El no puede contener ningun signo \n de puntuacion diferentes a caracteres";
                        }
                        return null;
                      },
                      false,
                      "Mi seguimiento",
                      "Titulo",
                      (String value) {
                        setState(() {
                          this._titulo = value;
                        });
                      },
                      context),
                  RaisedButton(
                    color: Color(0xffAD9B8A),
                    onPressed: guardar,
                    child: Text(
                      "Confirmar",
                      style: TextStyle(
                        color: Color(0xff5A5353),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  FutureOr<String> _onTimeout() {
    throw new Exception("No se pudo enviar la imagen. Intente Denuevo");
  }
}

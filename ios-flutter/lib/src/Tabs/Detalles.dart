import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/FireBase/firebStorage.dart';
import 'package:ios_flutter/src/Services/CrudConsulta.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:ios_flutter/src/Services/cloudFunctions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ios_flutter/src/Login/approvedMessage.dart';


class Detalles extends StatefulWidget {
  final Widget child;
  final User user;
  final Seguimiento seguimiento;

  Detalles(this.user, this.seguimiento, {Key key, this.child})
      : super(key: key);

  _DetallesState createState() => _DetallesState(user, seguimiento);
}

class _DetallesState extends State<Detalles> {
  User _user;
  Seguimiento _seguimiento;

  _DetallesState(this._user, this._seguimiento);

  @override
  Widget build(BuildContext context) {
    void addConsulta() async {
      print('press');
      var image = await Navigator.push(
          context,
          MaterialPageRoute<String>(
            builder: (context) => Camara(),
          ));

      if (image == null) {
        return;
      }

      var estado = await Connectivity().checkConnectivity();
      if (estado == ConnectivityResult.none) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("No esta conectado al internet"),
        ));
        return;
      }

      Consulta nva = new Consulta();

      var auth = new FireBaseAuth();

      var userAuth = await auth.getUser();

      FirebStorage fireSto = new FirebStorage();

      var date = DateTime.now().millisecondsSinceEpoch;
      String tituloSinEspacios =
          _seguimiento.name.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      String rutaGuardar = userAuth.uid +
          '/tracings/' +
          tituloSinEspacios +
          '/' +
          date.toString() +
          '.jpg';
      String imageURL = await fireSto.send(image, rutaGuardar);
      nva.date = date;
      nva.image = imageURL;
      nva.indicador = _seguimiento.name;
      nva.state = "LOADING";
      setState(() {
        _seguimiento.addConsulta(nva);
      });

      //Persisto
      var repoCons =
          new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
      repoCons.save(nva);

      //Envio a firestore
      CrudMethodsConsulta crudCons = new CrudMethodsConsulta();
      crudCons.addConsulta(nva, userAuth.uid);
    }

    var dia = new DateTime.fromMillisecondsSinceEpoch(_seguimiento.date);
    if (_seguimiento.colorChange == null || _seguimiento.colorChange == "") {
      _seguimiento.colorChange = "0";
    }
    if (_seguimiento.sizeChange == null) {
      _seguimiento.sizeChange = "";
    }
    if (_seguimiento.textureChange == null ||
        _seguimiento.textureChange == "") {
      _seguimiento.textureChange = "0";
    }
    String fecha = dia.day.toString() +
        '/' +
        dia.month.toString() +
        '/' +
        dia.year.toString() +
        '   ' +
        dia.hour.toString() +
        ':' +
        dia.minute.toString() +
        ':' +
        dia.second.toString();

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xffA64253),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Color(0xffE6CCBE), //change your color here
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.share, color: Color(0xffE6CCBE)),
            onPressed: () async {
              final Email email = Email(
                subject: 'Seguimiento de ${_seguimiento.name}',
                body: 'Este es el enlace de la imagen de mi seguimiento ${_seguimiento.name}: ${_seguimiento.consultas[_seguimiento.consultas.length-1].image}',
                recipients: ['${_user.emailDoctor}'],
                bcc: ['${_user.email}'],
               //attachmentPath: '${Image.network(_seguimiento.consultas[0].image)}'
               
);

await FlutterEmailSender.send(email);
await showDialog(
                context: this.context,
                builder: (context) => approvedMessage(context, "Correo enviado"))
            ;
            }
          ),
        ],
        title: Text(
          'Detalles',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Color(0xffE6CCBE),
              fontSize: MediaQuery.of(context).size.width / 18,
              fontFamily: 'Calibri Light'),
        ),
      ),
      body: CustomScrollView(
        
        slivers: <Widget>[
          SliverFixedExtentList(
              itemExtent: MediaQuery.of(context).size.height / .9,
              delegate: SliverChildListDelegate([
                Container(
                  color: Color(0xffE6CCBE),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 40,
                      ),
                      SizedBox(
                        child: Column(
                          children: <Widget>[
                            Text(
                              _seguimiento.name,
                              style: TextStyle(
                                  color: Color(0xffA07178),
                                  fontSize:
                                      MediaQuery.of(context).size.width / 13,
                                  fontFamily: 'Calibri Light'),
                              textAlign: TextAlign.center,
                            ),
                            Center(
                              child: Text(
                                fecha,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize:
                                        MediaQuery.of(context).size.width / 25,
                                    fontFamily: 'Calibri Light'),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height /
                                  3.4 /
                                  MediaQuery.of(context).size.height *
                                  40,
                            ),
                            Icon(
                              FontAwesomeIcons.fileMedicalAlt,
                              size: MediaQuery.of(context).size.height /
                                  3.4 /
                                  MediaQuery.of(context).size.height *
                                  200,
                              color: Color(0xffa64253),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height /
                                  3.4 /
                                  MediaQuery.of(context).size.height *
                                  40,
                            ),
                            Text('Estado ${_seguimiento.state}',
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize:
                                        MediaQuery.of(context).size.width / 20,
                                    fontFamily: 'Calibri Light')),
                          ],
                        ),
                        height: MediaQuery.of(context).size.height / 3.4,
                      ),

                      Container(
                        height: 3,
                        color: Color(0xffAD9B8A),
                      ),
                      SizedBox(height: 10),
//                       GridView.count(

//   crossAxisSpacing: 4.0,
//   crossAxisCount: 2,
//   children: <Widget>[
//     const Text('He\'d have you all unravel at the'),
//     const Text('Heed not the rabble'),
//     const Text('Sound of screams but the'),
//     const Text('Who scream'),
//     const Text('Revolution is coming...'),
//     const Text('Revolution, they...'),
//   ],
// ),
                      // Center(
                      //   child: Row(
                      //     children: <Widget>[
                      //       Column(
                      //         children: <Widget>[
                      //           Icon(
                      //             FontAwesomeIcons.expandArrowsAlt,
                      //             color: Color(0xffA07178),
                      //             size: 50,
                      //           )
                      //         ],
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Card(
                      //   margin:
                      //       EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      //   shape: RoundedRectangleBorder(
                      //     borderRadius: BorderRadius.circular(10.0),
                      //   ),
                      //   elevation: 5,
                      //   child: Container(
                      //     child: Column(
                      //       children: <Widget>[
                      //         Text(
                      //           "Crecimiento",
                      //           style: Theme.of(context).textTheme.title,
                      //         ),
                      //         Row(
                      //           children: <Widget>[
                      //             Icon(
                      //               Icons.arrow_upward,
                      //               size: 50,
                      //             ),
                      //             Text(
                      //               _seguimiento.sizeChange.toString() + '%',
                      //               style: Theme.of(context).textTheme.title,
                      //             ),
                      //             Flexible(
                      //               child: Text(
                      //                 "Aumento del ${_seguimiento.sizeChange} % en el tamaño respecto a la primer consulta.",
                      //                 overflow: TextOverflow.clip,
                      //                 textAlign: TextAlign.center,
                      //               ),
                      //             ),
                      //           ],
                      //         )
                      //       ],
                      //     ),
                      //   ),
                      // ),
                      Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Card(
                              margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width /
                                      2 /
                                      10,
                                  right: MediaQuery.of(context).size.width /
                                      2 /
                                      15,
                                  bottom: MediaQuery.of(context).size.height /
                                      4 /
                                      8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 0,
                              color: Color(0xffE6CCBE),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height:
                                    MediaQuery.of(context).size.height / 4.5,
                                child: Column(
                                  children: <Widget>[
                                    Icon(
                                      Icons.color_lens,
                                      size: MediaQuery.of(context).size.width /
                                          2.5 /
                                          1.6,
                                      color: Color(0xffa64253),
                                    ),
                                    Text(
                                      _seguimiento.colorChange,
                                      style: TextStyle(
                                          color: Color(0xffA07178),
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.5 /
                                              8,
                                          fontFamily: 'Calibri Light'),
                                    ),
                                    Text(
                                      "de variación cromática",
                                      overflow: TextOverflow.clip,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width /
                                                2.5 /
                                                12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width /
                                      2 /
                                      10,
                                  right: MediaQuery.of(context).size.width /
                                      2 /
                                      15,
                                  bottom: MediaQuery.of(context).size.height /
                                      4 /
                                      8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 0,
                              color: Color(0xffE6CCBE),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height:
                                    MediaQuery.of(context).size.height / 4.5,
                                child: Column(
                                  children: <Widget>[
                                    Icon(
                                      Icons.blur_circular,
                                      size: MediaQuery.of(context).size.width /
                                          2.5 /
                                          1.6,
                                      color: Color(0xffa64253),
                                    ),
                                    Text(
                                      _seguimiento.textureChange,
                                      style: TextStyle(
                                          color: Color(0xffA07178),
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.5 /
                                              8,
                                          fontFamily: 'Calibri Light'),
                                    ),
                                    Text(
                                      "de variación en textura",
                                      overflow: TextOverflow.clip,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width /
                                                2.5 /
                                                12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      Container(
                        height: 2,
                        color: Color(0xffAD9B8A),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Card(
                              margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width /
                                      2 /
                                      10,
                                  right: MediaQuery.of(context).size.width /
                                      2 /
                                      15,
                                  bottom: MediaQuery.of(context).size.height /
                                      4 /
                                      8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 0,
                              color: Color(0xffE6CCBE),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height:
                                    MediaQuery.of(context).size.height / 4.5,
                                child: Column(
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.expandArrowsAlt,
                                      size: MediaQuery.of(context).size.width /
                                          2.5 /
                                          1.6,
                                      color: Color(0xffa64253),
                                    ),
                                    Text(
                                      _seguimiento.sizeChange,
                                      style: TextStyle(
                                          color: Color(0xffA07178),
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.5 /
                                              8,
                                          fontFamily: 'Calibri Light'),
                                    ),
                                    Text(
                                      "de crecimiento",
                                      overflow: TextOverflow.clip,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width /
                                                2.5 /
                                                12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width /
                                      2 /
                                      10,
                                  right: MediaQuery.of(context).size.width /
                                      2 /
                                      15,
                                  bottom: MediaQuery.of(context).size.height /
                                      4 /
                                      8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              elevation: 0,
                              color: Color(0xffE6CCBE),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height:
                                    MediaQuery.of(context).size.height / 4.5,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    MaterialButton(
                                      color: Color(0xffAD9B8A),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: new Text(
                                        "Añadir seguimiento",
                                        style: TextStyle(
                                            color: Color(0xff5A5353),
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.5 /
                                                12,
                                            fontFamily: 'Calibri Light'),
                                      ),
                                      onPressed: addConsulta,
                                      splashColor: Color(0xffA64253),
                                    ),
                                    // SliverList(
                                    //   delegate: SliverChildBuilderDelegate(
                                    //       (BuildContext context, int index) {
                                    //     var consulta =
                                    //         _seguimiento.consultas[index];
                                    //     var dia = new DateTime
                                    //             .fromMillisecondsSinceEpoch(
                                    //         consulta.date);
                                    //     String fecha = dia.day.toString() +
                                    //         '/' +
                                    //         dia.month.toString() +
                                    //         '/' +
                                    //         dia.year.toString() +
                                    //         '   ' +
                                    //         dia.hour.toString() +
                                    //         ':' +
                                    //         dia.minute.toString() +
                                    //         ':' +
                                    //         dia.second.toString();
                                    //     return Card(
                                    //         margin: EdgeInsets.only(
                                    //             left: 10,
                                    //             right: 20,
                                    //             bottom: 20),
                                    //         shape: RoundedRectangleBorder(
                                    //           borderRadius:
                                    //               BorderRadius.circular(10.0),
                                    //         ),
                                    //         elevation: 5,
                                    //         child: Row(
                                    //           children: <Widget>[
                                    //             // Image.network(
                                    //             //   consulta.image,
                                    //             //   height: 100,
                                    //             //   width: 100,
                                    //             // ),
                                    //             SizedBox(
                                    //               width: 20,
                                    //             ),
                                    //             Text(
                                    //               fecha,
                                    //               style: Theme.of(context)
                                    //                   .textTheme
                                    //                   .title,
                                    //             ),
                                    //           ],
                                    //         ));
                                    //   },
                                    //       childCount:
                                    //           _seguimiento.consultas.length),
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      Card(
                        margin:
                            EdgeInsets.only(left: 10, right: 20, bottom: 20),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Padding(
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Consultas Realizadas",
                            style: Theme.of(context).textTheme.display1,
                            textAlign: TextAlign.center,    
                            
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
            
            
              ])
              ),
            SliverList(
                 
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
                  
              var consulta = _seguimiento.consultas[index];
               var dia = new DateTime
                                                .fromMillisecondsSinceEpoch(
                                            consulta.date);
                                        String fecha = dia.day.toString() +
                                            '/' +
                                            dia.month.toString() +
                                            '/' +
                                            dia.year.toString() +
                                            '   ' +
                                            dia.hour.toString() +
                                            ':' +
                                            dia.minute.toString() +
                                            ':' +
                                            dia.second.toString();
    
              return Container(

                  margin: EdgeInsets.only(left: 0, right: 0, bottom: 0),
                  decoration: new BoxDecoration(
                    color: Color(0xffE6CCBE) ,

                   
                  ),
                  child:
                  Card (
                      margin: EdgeInsets.only(top:10,
                                                left: 10,
                                                right: 20,
                                                bottom: 20),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            elevation: 5,
                  child:Row(

                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: <Widget>[
                      GestureDetector(
                        child: CachedNetworkImage(
                          imageUrl: consulta.image,
                          fit: BoxFit.scaleDown,
                          alignment: Alignment.center,
                          height: 100,
                          width: 100,
                        ),
                        onTap:(){
                          print('pressed');
                      
                        } ,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        fecha,
                        style: Theme.of(context).textTheme.title,
                      ),
                    ],
                  )
                  )
                  );
            }, childCount: _seguimiento.consultas.length),
          )
          ,

        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ios_flutter/src/Tabs/EditarPerfil.dart';

class Profile extends StatefulWidget {
  final Widget child;
  final User user;

  Profile(this.user, {Key key, this.child}) : super(key: key);

  _ProfileState createState() => _ProfileState(user);
}

class _ProfileState extends State<Profile> {
  _ProfileState(this._user);

  User _user;

  @override
  Widget build(BuildContext context) {
    FireBaseAuth _auth = new FireBaseAuth();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffA64253),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.signOutAlt, color: Color(0xffE6CCBE)),
            onPressed: () {
              
              _auth.logOut();
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ],
        title: Text(
          'Tu Perfil',
          textAlign: TextAlign.center,
          style: TextStyle(color: Color(0xffE6CCBE), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light'),
        ),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(FontAwesomeIcons.pen, color: Color(0xffE6CCBE)),
         onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EditarPerfil(_user)),
                  );
                },
        ),
      ),
      body: Container(
        color: Color(0xffE6CCBE),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text("${_user.firstName} ${_user.lastName}",
                  style: TextStyle(color: Color(0xffA07178), fontSize: MediaQuery.of(context).size.width / 10, fontFamily: 'Calibri Light')),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 25,
            ),
            Center(
              child: Text("${_user.email}",
                  style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
            ),

            
            SizedBox(
              height: MediaQuery.of(context).size.height / 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: MediaQuery.of(context).size.width / 4.5),
                Icon((_user.sex == 1)
                    ? FontAwesomeIcons.male
                    : FontAwesomeIcons.female,
                 color: Color(0xffA07178)),
                SizedBox(width: MediaQuery.of(context).size.width / 6),
                Text((_user.sex == 1) ? "Masculino" : "Femenino",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: MediaQuery.of(context).size.width / 4.5),
                Icon(FontAwesomeIcons.birthdayCake, color: Color(0xffA07178),),
                SizedBox(width: MediaQuery.of(context).size.width / 6),
                Text("${_user.birthDay}",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            
            SizedBox(
              height: MediaQuery.of(context).size.height / 30,
            ),
             Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: MediaQuery.of(context).size.width / 4.5),
                Icon(FontAwesomeIcons.userMd, color: Color(0xffA07178),),
                SizedBox(width: MediaQuery.of(context).size.width / 6),
                Text("${_user.nombreDoctor}",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                
                Text("${_user.emailDoctor}",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("¿Presenta lesiones en la piel",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
                Text("frecuentemente?",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 100,
            ),
            Center(
             child: Text((_user.commonIssue) ? "Sí" : "No",
                  style: TextStyle(color: Color(0xffA07178), fontSize: MediaQuery.of(context).size.width / 16, fontFamily: 'Calibri Light')),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 25,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("¿Algún familiar suyo ha sufrido de",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
                Text("cáncer en la piel?",
                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light')),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 100,
            ),
            Center(
              child: Text((_user.familySkinCancer) ? "Sí" : "No",
                style: TextStyle(color: Color(0xffA07178), fontSize: MediaQuery.of(context).size.width / 16, fontFamily: 'Calibri Light')),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 25,
            ),

            

          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Tabs/Detalles.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';

class Seguimientos extends StatefulWidget {
  final Widget child;
  final User user;

  Seguimientos(this.user, {Key key, this.child}) : super(key: key);

  _SeguimientosState createState() => _SeguimientosState(user);
}

class _SeguimientosState extends State<Seguimientos> {
  User _user;
  _SeguimientosState(this._user);

  @override
  Widget build(BuildContext context) {
    FireBaseAuth _auth = new FireBaseAuth();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffA64253),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.signOutAlt, color: Color(0xffE6CCBE)),
            onPressed: () {
              
              _auth.logOut();
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ],
        title: Text(
          'Tus Seguimientos',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Color(0xffE6CCBE),
              fontSize: MediaQuery.of(context).size.width / 18,
              fontFamily: 'Calibri Light'),
        ),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Color(0xffA64253),
        child: (_user.seguimientos.length == 0)
            ? Center(
                child: Text(
                  "Todavia no tienes seguimientos",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xffE6CCBE),
                      fontSize: MediaQuery.of(context).size.width / 18,
                      fontFamily: 'Calibri Light'),
                ),
              )
            : ListView.builder(
                itemCount: _user.seguimientos.length,
                padding: EdgeInsets.only(bottom: 5),
                itemBuilder: (BuildContext context, int index) {
                  var seguimiento = _user.seguimientos[index];
                  var dia =
                      new DateTime.fromMillisecondsSinceEpoch(seguimiento.date);
                  String fecha = dia.day.toString() +
                      '/' +
                      dia.month.toString() +
                      '/' +
                      dia.year.toString() +
                      '   ' +
                      dia.hour.toString() +
                      ':' +
                      dia.minute.toString() +
                      ':' +
                      dia.second.toString();
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute<String>(
                            builder: (context) =>
                                Detalles(_user, seguimiento),
                          ));
                    },
                    child: Hero(
                      child: Card(
                        color: Color(0xffE6CCBE),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        margin: EdgeInsets.all(10),
                        elevation: 5,
                        child: Container(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 18,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    seguimiento.name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 18, fontFamily: 'Calibri Light'),
                                  ),
                                  Text(
                                    fecha,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Color(0xff5A5353), fontSize: MediaQuery.of(context).size.width / 26, fontFamily: 'Calibri Light'),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      tag: seguimiento.name,
                    ),
                  );
                },
              ),
      ),
    );
  }
}

import 'dart:io';
import 'dart:developer';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';

import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/Login/warningMessage.dart';
import 'package:ios_flutter/src/firstPage.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:ios_flutter/src/FireBase/firebStorage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

import 'package:firebase_storage/firebase_storage.dart';

class FirstTimePage extends StatefulWidget {
  final Widget child;

  final User user;

  FirstTimePage(this.user, {Key key, this.child}) : super(key: key);

  _FirstTimePage createState() => _FirstTimePage(user);
}

class _FirstTimePage extends State<FirstTimePage> {
  _FirstTimePage(this._user);

  User _user;

  FireBaseAuth _auth = new FireBaseAuth();

  final _formKey = GlobalKey<FormState>();

  /// Maneja el estado de boton de la pregunta de lesiones frecuentes
  int _radioButtonState1;

  /// Maneja el estado de boton de la pregunta de antecedentes familiares
  int _radioButtonState2;

  /// True en el caso de que si sean lesiones frecuentes, false de lo contrario
  bool _lesionesFrecuentes;

  ///True en el caso de que si tenga antecedentes familiares.
  bool _presedentesFamiliares;

  String _imageURL;

  void manejoLesionesFrecuentes(int estado) {
    setState(() {
      _radioButtonState1 = estado;
    });
    switch (estado) {
      case 0:
        _lesionesFrecuentes = true;
        break;
      case 1:
        _lesionesFrecuentes = false;
        break;
    }
  }

  void manejoAntecedentes(int estado) {
    setState(() {
      _radioButtonState2 = estado;
    });
    switch (estado) {
      case 0:
        _presedentesFamiliares = true;
        break;
      case 1:
        _presedentesFamiliares = false;
        break;
    }
  }

  void guardar() async {
    CrudMethodsUser crud = new CrudMethodsUser();
    FirebaseUser miUser = await _auth.getUser();
    if (_lesionesFrecuentes == null) {
      showDialog(
        builder: ((context) => warningMessage(
            context, "Por favor llene el campo de lesiones frecuentes")),
        context: context,
      );
      return;
    }
    if (_presedentesFamiliares == null) {
      showDialog(
        builder: ((context) => warningMessage(
            context, "Por favor llene el campo de antecedentes familiares")),
        context: context,
      );
      return;
    }
    if (_imageURL == null) {
      showDialog(
        builder: ((context) =>
            warningMessage(context, "Por favor tome una foto de su piel")),
        context: context,
      );
      return;
    }
    _user.skinToneImage = _imageURL;
    _user.familySkinCancer = _presedentesFamiliares;
    _user.commonIssue = _lesionesFrecuentes;
    var repo = new FuturePreferencesRepository<User>(new UserDesSer());
    repo.removeAll();
    repo.save(_user);

  
    String rutaInterna = miUser.uid + '/' + "skinToneImage/";
    String rutaGuardar = rutaInterna + '/' + miUser.uid + ".jpg";
    FirebStorage fireSto = new FirebStorage();
    final String downloadurl = await fireSto.send(_imageURL, rutaGuardar);
    
     crud.addHistory(_lesionesFrecuentes, _presedentesFamiliares,
              downloadurl, miUser.uid);
          log('$downloadurl');

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => FirstPage(_user)),
              ModalRoute.withName('/'));

   
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffA64253),
        child: ListView(
          children: <Widget>[
            Card(
              color: Color(0xffE6CCBE),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              child: Container(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 30, right: MediaQuery.of(context).size.width / 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: MediaQuery.of(context).size.height / 20),
                    Text(
                      "Hola " + _user.firstName + ",",
                      style: TextStyle(
                          color: Color(0xffA07178),
                          fontSize: MediaQuery.of(context).size.height / 15,
                          fontFamily: 'Calibri Light'),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 30,
                    ),
                    Text(
                      "Para obtener resultados mas precisos, queremos hacerte unas preguntas más:",
                      style: TextStyle(
                          color: Color(0xff5A5353),
                          fontSize: MediaQuery.of(context).size.height / 30,
                          fontFamily: 'Calibri Light'),
                      textAlign: TextAlign.center,
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 30,
                          ),
                          Text(
                            "¿Presentas lesiones en la piel frecuentemente?",
                            style: TextStyle(
                                color: Color(0xffA07178),
                                fontSize: MediaQuery.of(context).size.height / 33,
                                fontFamily: 'Calibri Light'),
                            textAlign: TextAlign.center,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                groupValue: _radioButtonState1,
                                value: 0,
                                onChanged: manejoLesionesFrecuentes,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "Sí",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Radio(
                                groupValue: _radioButtonState1,
                                value: 1,
                                onChanged: manejoLesionesFrecuentes,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "No",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 30,
                          ),
                          Text(
                            "¿Algún familiar tuyo ha sufrido de cáncer en la piel?",
                            style: TextStyle(
                                color: Color(0xffA07178),
                                fontSize: MediaQuery.of(context).size.height / 33,
                                fontFamily: 'Calibri Light'),
                            textAlign: TextAlign.center,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                groupValue: _radioButtonState2,
                                value: 0,
                                onChanged: manejoAntecedentes,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "Sí",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Radio(
                                groupValue: _radioButtonState2,
                                value: 1,
                                onChanged: manejoAntecedentes,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "No",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 30),
                          Text(
                            "Sube una foto de la zona de tu piel que consideras representa de mejor manera tu tono natural. Trata de que la zona de la piel cubra toda la camara.",
                            style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: MediaQuery.of(context).size.height / 37,
                                fontFamily: 'Calibri Light'),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 45,
                          ),
                          (_imageURL == null)
                              ? Text("")
                              : SizedBox(
                                  child: Image.file(File(_imageURL)),
                                  width: MediaQuery.of(context).size.width / 3,
                                  height: MediaQuery.of(context).size.height / 30,
                                ),
                          GestureDetector(
                            child: Icon(
                              Icons.photo_camera,
                              size: MediaQuery.of(context).size.height / 13,
                              color: Color(0xffA07178),
                            ),
                            onTap: () async {
                              _imageURL = await Navigator.push(
                                  context,
                                  MaterialPageRoute<String>(
                                    builder: (context) => Camara(),
                                  ));
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                    MaterialButton(
                      color: Color(0xffAD9B8A),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: new Text(
                        "Guardar",
                        style: TextStyle(
                            color: Color(0xff5A5353),
                            fontSize: MediaQuery.of(context).size.height / 37,
                            fontFamily: 'Calibri Light'),
                      ),
                      onPressed: guardar,
                      splashColor: Color(0xffA64253),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 37,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

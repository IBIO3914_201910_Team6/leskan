import 'dart:async';
import 'dart:io';
import 'package:ios_flutter/src/Login/warningMessage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

import 'package:firebase_storage/firebase_storage.dart';

class FirebStorage {
  
  Future<String> send(imagen, rutaGuardar) async {
    StorageReference reference =
        FirebaseStorage.instance.ref().child(rutaGuardar);

    final File file = File(imagen);
    StorageUploadTask uploadTask = reference.putFile(file);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete.then((value){
      return value;
    });
  
    final String url = await storageTaskSnapshot.ref.getDownloadURL();
    return url;
  
    

  
  }
}

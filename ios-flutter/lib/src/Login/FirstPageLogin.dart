import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Login/Login.dart';
import 'package:ios_flutter/src/Login/SignIn.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirstTimeLogin extends StatefulWidget {
  final Widget child;
  final User user;

  FirstTimeLogin(this.user, {Key key, this.child}) : super(key: key);

  _FirstTimeLoginState createState() => _FirstTimeLoginState(user);
}

class _FirstTimeLoginState extends State<FirstTimeLogin> {
  User _user;
  _FirstTimeLoginState(this._user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffA64253),
        child: ListView(
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height / 3 - 30,),
            Text('Skin Scanner',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color(0xffE6CCBE),
                    fontSize: MediaQuery.of(context).size.height / 12,
                    fontFamily: 'Calibri Light')),
            Container(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 10,
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    minWidth: MediaQuery.of(context).size.width / 2.4,
                    color: Color(0xffE6CCBE),
                    child: new Text(
                      "Iniciar Sesión",
                      style: TextStyle(
                          color: Color(0xff5A5353),
                          fontSize: MediaQuery.of(context).size.height / 30,
                          fontFamily: 'Calibri Light'),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Login(_user)));
                    },
                    splashColor: Color(0xffA64253),
                  ),
                  
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 35,
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    minWidth: MediaQuery.of(context).size.width / 2.4,
                    color: Color(0xffE6CCBE),
                    child: new Text(
                      "Registrarse",
                      style: TextStyle(
                          color: Color(0xff5A5353),
                          fontSize: MediaQuery.of(context).size.height / 30,
                          fontFamily: 'Calibri Light'),
                    ),
                    onPressed: () => {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => SignIn()))
                        },
                    splashColor: Color(0xffA64253),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

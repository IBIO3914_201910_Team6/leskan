import 'dart:async';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:validate/validate.dart';
import 'package:ios_flutter/src/Login/approvedMessage.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:connectivity/connectivity.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => new _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();

  String _email;

  String _password;

  String _firstName;

  String _lastName;

  String _nombreDoctor;

  String _emailDoctor;

  int _sexo;

  CrudMethodsUser _services = new CrudMethodsUser();

  DateTime _birthDay = new DateTime.now();

  ///controller para el texto de la contrasenia 2
  TextEditingController _textController = new TextEditingController();

  /// valida el campo del Email
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'El e-mail no es válido.';
    }
    return null;
  }

  String _validateEmailDoctor(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'El e-mail no es válido.';
    }
    return null;
  }

  /// Valida el campo del Password
  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'La contraseña debe tener por lo menos 8 caracteres.';
    }
    return null;
  }

  ///valida todo el registro y lo guarda en el estado
  Future submit() async {
    // First validate form.
    var estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("No esta conectado al internet"),
      ));
      return;
    }
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      try {
        // creates the firebase user
        FireBaseAuth auth = new FireBaseAuth();
        var user = await auth.createUser(_email, _password);
        //creates Map to add to firestore

        String month = _birthDay.month.toString();
        if (month.length == 1) {
          month = "0" + month;
        }

        String day = _birthDay.day.toString();
        if (day.length == 1) {
          day = "0" + day;
        }

        String nacimiento = _birthDay.year.toString() + '-' + month + '-' + day;

        Map<String, dynamic> data = {
          'email': this._email,
          'firstName': this._firstName,
          'lastName': this._lastName,
          'sex': this._sexo,
          'birthDate': nacimiento,
          'skinTone': -1,
          'skinToneImage': '',
          'nombreDoctor' : this._nombreDoctor,
          'emailDoctor' : this._emailDoctor,
        };
        // adds it to the database.
        _services.addUser(data, user.uid);
        //success
        await showDialog(
                context: this.context,
                builder: (context) => approvedMessage(context, "¡Registro exitoso!"))
            .then((value) {
          Navigator.pop(context);
        });
      } catch (e) {
        showDialog(
          context: this.context,
          builder: (context) => errorMessage(context, e.message),
        );
      }
    }
  }

  /// selecciona el dia de nacimiento
  Future seleccioarDia(BuildContext context) async {
    
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDay,
        firstDate: DateTime(1910, 1),
        lastDate: DateTime.now());
        builder: (BuildContext context, Widget child){
          return FittedBox(
            child: Theme(
              child: child,
              data: ThemeData(
                primaryColor: Color(0xffE6CCBE),
                ),
                ),
        );
        };
  

    if (picked != null && picked != _birthDay)
      setState(() {
        _birthDay = picked;
      });
  }

  /// Maneja el estado del RadioBotton
  void handleRadioValueChange(int value) {
    setState(() {
      _sexo = value;
    });
  }

  ///crea el view del registro
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffA64253),
        child: ListView(
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height / 20),
            Text('Skin Scanner',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color(0xffE6CCBE),
                    fontSize: MediaQuery.of(context).size.height / 13,
                    fontFamily: 'Calibri Light')),
            Card(
              color: Color(0xffE6CCBE),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              child: Column(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height / 50),
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 15, right: MediaQuery.of(context).size.width / 15),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 40,
                          ),
                          BuildTextFormField(
                              null, null, false, 'Nombre', 'Nombre',
                              (String value) {
                            this._firstName = value;
                          }, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 40,
                          ),
                          BuildTextFormField(
                              null, null, false, 'Apellido', 'Apellido',
                              (String value) {
                            this._lastName = value;
                          }, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 35,
                          ),
                          Text(
                            "Sexo",
                            style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: MediaQuery.of(context).size.height / 30,
                                fontFamily: 'Calibri Light'),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                groupValue: _sexo,
                                value: 0,
                                onChanged: handleRadioValueChange,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "Femenino",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize:  MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                              SizedBox(
                                width:  MediaQuery.of(context).size.height / 20,
                              ),
                              Radio(
                                groupValue: _sexo,
                                value: 1,
                                onChanged: handleRadioValueChange,
                                activeColor: Color(0xff5A5353),
                              ),
                              Text(
                                "Masculino",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 35,
                                    fontFamily: 'Calibri Light'),
                              ),
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 30),
                          Text(
                            "Fecha de Nacimiento",
                            style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: MediaQuery.of(context).size.height / 30,
                                fontFamily: 'Calibri Light'),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 60,
                          ),
                          Row(
                            children: <Widget>[
                              SizedBox(width: MediaQuery.of(context).size.height / 25),
                              MaterialButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                color: Color(0xffAD9B8A),
                                onPressed: () => seleccioarDia(context),
                                child: Text("Seleccionar",
                                    style: TextStyle(
                                        color: Color(0xff5A5353),
                                        fontSize: MediaQuery.of(context).size.height / 35,
                                        fontFamily: 'Calibri Light')),
                                splashColor: Color(0xffA64253),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.height / 15,
                              ),
                              Container(
                                child: (_birthDay == null)
                                    ? Text('Seleccionar')
                                    : Text(
                                        '${_birthDay.day}/${_birthDay.month}/${_birthDay.year}',
                                        style: TextStyle(
                                            color: Color(0xff5A5353),
                                            fontSize: MediaQuery.of(context).size.height / 35,
                                            fontFamily: 'Calibri Light')),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 20,
                          ),
                          BuildTextFormField(null, this._validateEmail, false,
                              'you@mail.com', 'E-mail', (String value) {
                            this._email = value;
                          }, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 40,
                          ),
                          BuildTextFormField(_textController, _validatePassword,
                              true, 'Password', 'Password', (String value) {
                            this._password = value;
                          }, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 40,
                          ),
                          BuildTextFormField(null, (String value) {
                            if (this._textController.text != value) {
                              return 'Las contraseñas deben ser iguales';
                            }
                          }, true, 'Contraseña', 'Repita Contraseña',
                              (String value) {}, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 10,
                          ),
                           Container(
                        height: 3,
                        color: Color(0xffAD9B8A),
                      ),
                      SizedBox(
                            height: MediaQuery.of(context).size.height / 35,
                          ),
                          Text(
                            "Información de su dermatólogo",
                            style: TextStyle(
                                color: Color(0xffA64253),
                                fontSize: MediaQuery.of(context).size.height / 30,
                                fontFamily: 'Calibri Light'),
                          ),
                          BuildTextFormField(
                              null, null, false, 'Nombre del médico', 'Nombre del médico',
                              (String value) {
                            this._nombreDoctor = value;
                          }, context),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 60,
                          ),
                          BuildTextFormField(null, this._validateEmailDoctor, false,
                              'you@mail.com', 'E-mail del médico', (String value) {
                            this._emailDoctor = value;
                          }, context),
                          
                          MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Color(0xffAD9B8A),
                            onPressed: this.submit,
                            child: Text("Registrarse",
                                style: TextStyle(
                                    color: Color(0xff5A5353),
                                    fontSize: MediaQuery.of(context).size.height / 32,
                                    fontFamily: 'Calibri Light')),
                            splashColor: Color(0xffA64253),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 30,)
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

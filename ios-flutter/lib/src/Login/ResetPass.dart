import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Login/warningMessage.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:validate/validate.dart';

class ResetPassword extends StatefulWidget {
  final Widget child;

  ResetPassword({Key key, this.child}) : super(key: key);

  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();

  String _email;

  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  void enviarEmail() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();
      FirebaseAuth.instance.sendPasswordResetEmail(email: _email).then((_) {
        showDialog(
          context: this.context,
          builder: (context) => warningMessage(context,
              "Se envio un correo de verificacion. \n Revise su bandeja de entrada."),
        ).then((_) {
          Navigator.pop(context);
        });
      }).catchError((e) {
        showDialog(
          context: this.context,
          builder: (context) => errorMessage(context, e.message),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffA64253),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 130),
            Hero(
              tag: "intro",
              child: Container(
                constraints: BoxConstraints(maxWidth: 200, maxHeight: 200),
                child: Text('Skin Scanner',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color(0xffE6CCBE),
                    fontSize: 50,
                    fontFamily: 'Calibri Light')),
              ),
            ),
            Text(
              "Restablecer Contraseña",
              style: TextStyle(
                color: Color(0xffdbb9a6),
                fontSize: 30,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Color(0xffE6CCBE), 
              child: Container(
                
                padding: EdgeInsets.only(left: 40, right: 40),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      BuildTextFormField(null, _validateEmail, false,
                          "you@mail.com", "E-mail", (value) {
                        setState(() {
                          _email = value;
                        });
                      }, context),
                      SizedBox(
                        height: 40,
                      ),
                     MaterialButton(
                       shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        color: Color(0xffAD9B8A),
                        onPressed: enviarEmail,
                        child: new Text(
                          "Enviar Email", 
                          style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: 20,
                                fontFamily: 'Calibri Light'),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

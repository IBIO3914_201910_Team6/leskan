import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:ios_flutter/src/Login/ResetPass.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Services/CrudSeguimiento.dart';
import 'package:validate/validate.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:ios_flutter/src/firstTimePage.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/firstPage.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';

class Login extends StatefulWidget {
  final User user;

  Login(this.user);

  @override
  _Login createState() => _Login(user);
}

class _Login extends State<Login> {
  _Login(this._user);

  CrudMethodsSeguimiento _servicesSegimiento = new CrudMethodsSeguimiento();
  CrudMethodsUser _servicesUser = new CrudMethodsUser();

  User _user;

  final _formKey = GlobalKey<FormState>();

  String _email;

  String _password;

  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  void submit() async {
    // revisa si estas conectado a internet

    var estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      showDialog(
        context: this.context,
        builder: (context) =>
            errorMessage(context, "No esta conectado a internet"),
      );
      return;
    }

    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.

      // autenticar
      var auth = new FireBaseAuth();

      auth.login(_email, _password).then((value) {
        // obtener informacion basica
        _servicesUser.setUser(value.uid, _user).then((user) {
          _servicesSegimiento.getSeguimientos(value.uid, user).then((user2) {
            _user = user2;
            var repo = new FuturePreferencesRepository<User>(new UserDesSer());
            var repoSeg = new FuturePreferencesRepository<Seguimiento>(
                new SeguimientoDesSer());
            var repoCons =
                new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());

            //persistir
            repo.removeAll();
            repo.save(_user);
            _user.seguimientos.forEach((seg) {
              repoSeg.save(seg);
              seg.consultas.forEach((con) {
                repoCons.save(con);
              });
            });

            // es primera vez?
            if (_user.skinToneImage == '') {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FirstTimePage(_user)));
            } else {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FirstPage(_user)));
            }
          });
        });
      }).catchError((error) {
        showDialog(
          context: this.context,
          builder: (context) => errorMessage(context, error.message),
        );
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
      final deviceRatio = size.width / size.height;
    return Scaffold(
      body: Container(
        color: Color(0xffA64253),
        child: ListView(
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height / 6),
            Text('Skin Scanner',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color(0xffE6CCBE),
                    fontSize: MediaQuery.of(context).size.width / 8,
                    fontFamily: 'Calibri Light')),
            Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 50, right: MediaQuery.of(context).size.width / 50),
              child: Card(
                color: Color(0xffE6CCBE),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(MediaQuery.of(context).size.width / 25),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        BuildTextFormField(null, _validateEmail, false,
                            'you@mail.com', 'E-mail', (String value) {
                          this._email = value;
                        }, context),
                        SizedBox(
                          height: MediaQuery.of(context).size.width / 20
                        ),
                        BuildTextFormField(null, _validatePassword, true,
                            'Password', 'Password', (String value) {
                          this._password = value;
                        }, context),
                        SizedBox(
                          height: MediaQuery.of(context).size.width / 20,
                        ),
                        MaterialButton(
                          minWidth:  MediaQuery.of(context).size.width / 2.4,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Color(0xffAD9B8A),
                          child: new Text(
                            "Iniciar Sesión",
                            style: TextStyle(
                                color: Color(0xff5A5353),
                                fontSize: MediaQuery.of(context).size.width / 17,
                                fontFamily: 'Calibri Light'),
                          ),
                          onPressed: this.submit,
                          splashColor: Color(0xffA64253),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.width / 60,
            ),
            Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  child: Text(
                    'Olvidaste la contraseña? Haz click aca.',
                    style: TextStyle(color: Color(0xffE6CCBE)),
                  ),
                  onTap: () async {
                    var estado = await Connectivity().checkConnectivity();
                    if (estado == ConnectivityResult.none) {
                      showDialog(
                        context: this.context,
                        builder: (context) => errorMessage(
                            context, "No esta conectado a internet"),
                      );
                      return;
                    }
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResetPassword()));
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:pref_dessert/pref_dessert.dart';

class Consulta {
  String city;
  String country;
  int date;
  String image;
  String state;
  String indicador;
}

class ConsultaDesSer extends DesSer<Consulta> {
  @override
  Consulta deserialize(String s) {
     Map map = json.decode(s);
     var cons = new Consulta();

     cons.city = map['city'];
     cons.country = map['country'];
     cons.date = map['date'];
     cons.image = map['image'];
     cons.state = map['state'];
     cons.indicador = map['indicador'];
    return cons;
  }

  @override
  String get key => 'CONSULTA';

  @override
  String serialize(Consulta cons) {

    Map jCons = {
      'city': cons.city,
      'country': cons.country,
      'date': cons.date,
      'image': cons.image,
      'state': cons.state,
      'indicador': cons.indicador,
    };
    return json.encode(jCons);
  }
  
}
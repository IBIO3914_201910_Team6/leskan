import 'dart:convert';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:pref_dessert/pref_dessert.dart';
class User {
  String birthDay;

  String email;

  String firstName;

  String lastName;

  String nombreDoctor;

  String emailDoctor;

  int skinTone;
  
  String skinToneImage;

  int sex;

  bool commonIssue;

  bool familySkinCancer;

  List <Seguimiento> seguimientos = new List();

  addSeguimiento(Seguimiento seguimiento){
    seguimientos.add(seguimiento);
  }
}

class UserDesSer extends DesSer<User> {
  @override
  User deserialize(String s) {
    var map = json.decode(s);
    User user = new User();
    user.skinTone = map['skinTone'] as int;
    user.birthDay = map['birthDate'] as String;
    user.email = map['email'] as String;
    user.firstName = map['firstName'] as String;
    user.lastName = map['lastName'] as String;
    user.sex = map['sex'] as int;
    user.skinToneImage = map['skinToneImage'] as String;
    user.commonIssue = map['commonIssue'] as bool;
    user.familySkinCancer = map['familySkinCancer'] as bool;
    user.nombreDoctor = map['nombreDoctor'] as String;
    user.emailDoctor = map['emailDoctor'] as String;

    return user;
  }

  @override
  String get key => 'USER';

  @override
  String serialize(User user) {
    Map jUser = {
      'email': user.email,
      'firstName': user.firstName,
      'lastName': user.lastName,
      'sex': user.sex,
      'birthDate': user.birthDay,
      'skinTone': user.skinTone,
      'skinToneImage': user.skinToneImage,
      'familySkinCancer' : user.familySkinCancer,
      'commonIssue':user.commonIssue,
      'emailDoctor' : user.emailDoctor,
      'nombreDoctor' : user.nombreDoctor,
    };
    return json.encode(jUser);
  }
}

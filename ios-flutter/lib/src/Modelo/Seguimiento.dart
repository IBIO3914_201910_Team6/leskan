import 'dart:convert';

import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:pref_dessert/pref_dessert.dart';

class Seguimiento{

  String state;

  String colorChange;

  String sizeChange;

  String textureChange;

  String name;

  int date;

  Seguimiento();

  List <Consulta> consultas = new List<Consulta>(); 

  addConsulta(Consulta consulta){
    consultas.add(consulta);
  }

}

class SeguimientoDesSer extends DesSer<Seguimiento> {
  @override
  Seguimiento deserialize(String s) {
    Map map = json.decode(s);
    var seg = new Seguimiento();

    seg.state = map['state'] as String;
    seg.colorChange = map['colorChange'] as String;
    seg.sizeChange = map['sizeChange'] as String;
    seg.textureChange = map['textureChange'] as String;
    seg.name = map['name'] as String;
    seg.date = map['date'] as int;
    return seg;
  }

  @override
  String get key => 'SEGUIMIENTO';

  @override
  String serialize(Seguimiento seg) {
    Map jSeg = {
      'state': seg.state,
      'colorChange': seg.colorChange,
      'sizeChange': seg.sizeChange,
      'textureChange': seg.textureChange,
      'name': seg.name,
      'date': seg.date,
    };
    return json.encode(jSeg);
  }
  
}
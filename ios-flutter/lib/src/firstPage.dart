import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Tabs/Seguimientos.dart';
import 'package:ios_flutter/src/Tabs/Profile.dart';
import 'package:ios_flutter/src/Tabs/NuevaConsulta.dart';


class FirstPage extends StatefulWidget {
  final Widget child;

  final User user;

  FirstPage(this.user, {Key key, this.child}) : super(key: key);

  _FirstPageState createState() => _FirstPageState(user);
}

class _FirstPageState extends State<FirstPage> {
  _FirstPageState(this._user);

  User _user;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 3,
      child: new Scaffold(
        body: TabBarView(
          children: [
            new Seguimientos(_user),
            new NuevaConsulta(_user),
            new Profile(_user),
          ],
        ),
        bottomNavigationBar: new Material(
          color: Color(0xffA64253),
                  child: new TabBar(
            tabs: [
              Tab(
                child: Text( "Seguimientos", style: TextStyle(color: Color(0xffE6CCBE),
                fontSize: MediaQuery.of(context).size.width / 27,),
                textAlign: TextAlign.center,),
                icon: new Icon(Icons.description, color: Color(0xffE6CCBE)),
              ),
              Tab(
                
                child: Text( "Nueva Consulta", style: TextStyle(color: Color(0xffE6CCBE),
                fontSize: MediaQuery.of(context).size.width / 27,),
                textAlign: TextAlign.center,),
                icon: new Icon(Icons.photo_camera, color: Color(0xffE6CCBE)),
              ),
              Tab(
                child: Text( "Perfil", style: TextStyle(color: Color(0xffE6CCBE),
                fontSize: MediaQuery.of(context).size.width / 27,),
                textAlign: TextAlign.center,),
                icon: new Icon(Icons.person, color: Color(0xffE6CCBE)),
              ),
        
            ],
            labelColor: Color(0xffE6CCBE),
            unselectedLabelColor: Color(0xffE6CCBE),
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.all(5.0),
            indicatorColor: Color(0xffE6CCBE),
          ),
        ),
        backgroundColor: Color(0xffE6CCBE),
      ),
    );
  }
}
